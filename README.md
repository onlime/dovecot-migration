# dovecot-migration

Scripts for migrating mailboxes from Cyrus to Dovecot IMAP Server.

## Install

Manual installation:

```bash
$ git clone git@gitlab.com:onlime/dovecot-migration.git
$ cd dovecot-migration
$ python3 -m venv venv
$ . venv/bin/activate
(venv) $ pip install -r requirements.txt
```

## Usage

### Command help

Main migration script `migrate.py` is pretty powerful, check `migrate.py --help` for help:

```
usage: migrate.py [-h] [--users-file USERS_FILE] [--username USERNAME] [--revert] [--dry-run] [--logdir LOGDIR] [--debug] [--verbose]
                  [--skip-proxy-change] [--skip-presync] [--skip-postsync] [--skip-sync-sieve]

optional arguments:
  -h, --help            show this help message and exit
  --users-file USERS_FILE
                        File which contains the usernames to migrate (one per line). (Default: users.list)
  --username USERNAME   If given only this user will be migrated instead of the content of the users file. Source and destination username must be the
                        same.
  --revert              Do a revert migration of the specified user(s).
  --dry-run             Only log what would happen and show sieve errors. Should be used with --debug
  --logdir LOGDIR       Logdir path (Default: /tmp)
  --debug               Enable debug mode
  --verbose             Also print all log entries
  --skip-proxy-change   Skip migration to proxied IMAP server (Dovecot), only migrate data without going live.
  --skip-presync        Skip first data synchronization step, only execute the second incremental one
  --skip-postsync       Skip final data synchronization step
  --skip-sync-sieve     Skip synchronization of Sieve script
```

### Examples

TBD.

## About dovecot-migration

The project is hosted on [GitLab](https://gitlab.com/onlime/dovecot-migration).
Please use the project's [issue tracker](https://gitlab.com/onlime/dovecot-migration/issues) if you find bugs.

myisam-to-innodb was written by [Philip Iezzi](https://gitlab.com/piezzi) for [Onlime GmbH](https://www.onlime.ch).
