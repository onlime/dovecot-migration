-- -----------------------------------------------------
-- Schema dovecot-migration
-- -----------------------------------------------------
USE `dovecot-migration` ;

-- -----------------------------------------------------
-- Table `dovecot-migration`.`migrations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `dovecot-migration`.`migrations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `source_user` VARCHAR(255) NOT NULL DEFAULT '',
  `dest_user` VARCHAR(255) NOT NULL DEFAULT '',
  `migrate_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `run_id` CHAR(8) DEFAULT NULL,
  `presync_time` FLOAT NOT NULL DEFAULT '0',
  `sievesync_time` FLOAT NOT NULL DEFAULT '0',
  `proxychange_time` FLOAT NOT NULL DEFAULT '0',
  `postsync_time` FLOAT NOT NULL DEFAULT '0',
  `total_time` FLOAT NOT NULL DEFAULT '0',
  `storage_kb` INT NULL DEFAULT NULL,
  `reverse` TINYINT(1) NOT NULL DEFAULT false,
  PRIMARY KEY (`id`),
  INDEX `idx_username` (`username` ASC)
  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
