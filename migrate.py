#!/usr/bin/env python3
import argparse
from helper.setup_logging import configure_logging, RUN_ID
from helper.file import File
from helper.profiling import Profiler, Profiling
from os import path
from requests import post
from simpleflock import SimpleFlock
from sync_data import DataMigrator
from sync_sieve import SieveMigrator
from change_proxy import change_proxy, kick_imap_connection
from settings import get_settings

cfg = get_settings()

class Migrator(object):

    def __init__(self, logger: object, revert: bool = False, dry_run: bool = False, skip_proxy_change: bool = False, skip_presync: bool = False, skip_postsync: bool = False, skip_sync_sieve: bool = False):
        self.logger = logger
        self.revert = revert
        self.dry_run = dry_run
        self.skip_proxy_change = skip_proxy_change
        self.skip_presync = skip_presync
        self.skip_postsync = skip_postsync
        self.skip_sync_sieve = skip_sync_sieve

    def run_migration(self, users_file: str):
        file_obj = File(users_file)
        lines = file_obj.get_lines()
        for line in lines:
            users = line.split()
            source_user = users[0]
            dest_user = users[1] if len(users) > 1 else source_user
            self.migrate_user(source_user, dest_user, users_file)

    def migrate_user(self, source_user: str, dest_user: str, users_file: str):
        # ensure this account has not already been migrated in a previous run
        file_done = File(users_file + '.done')
        if not self.revert and file_done.line_in_file(source_user):
            self.logger.warn('Mailaccount {} was already migrated. Skipping.'.format(source_user))
            return False
        
        # Setup profiler
        profiler = Profiler(cfg['profiler'], source_user, dest_user, RUN_ID, int(self.revert))

        log_revert = 'reverse ' if self.revert else ''
        self.logger.info('###### START {}migration of user {} ######'.format(log_revert, source_user))

        data_migrator = DataMigrator(source_user, dest_user, self.logger, self.dry_run)
        if self.skip_presync:
            self.logger.info('Skipping data pre-sync for user {}'.format(source_user))
        else:
            self.logger.info('Running {}data pre-sync for user {}'.format(log_revert, source_user))
            with Profiling(profiler, 'presync'):
                data_migrator.migrate_data(not self.revert, self.revert)
            self.logger.info('{}Pre-Sync for user {} finished'.format(log_revert, source_user))

        if self.skip_sync_sieve or self.revert:
            self.logger.info('Skipping sync of Sieve script for user {}'.format(source_user))
        else:
            self.logger.info('Running sieve sync for user {}'.format(source_user))
            with Profiling(profiler, 'sievesync'):
                sieve_migrator = SieveMigrator(source_user, dest_user, self.logger, self.dry_run)
                sieve_migrator.migrate_active()
                sieve_migrator.logout()
            self.logger.info('Sieve sync for user {} finished'.format(source_user))

        if self.skip_proxy_change:
            self.logger.info('Skipping proxy_server change for user {}'.format(source_user))
        else:
            self.logger.info('Running {}proxy change for user {}'.format(log_revert, source_user))
            proxy_key_new = 'dest' if not self.revert else 'source'
            with Profiling(profiler, 'proxychange'):
                change_proxy(source_user, proxy_key_new, self.dry_run, self.logger)
            self.logger.info('Proxy {}change for user {} finished'.format(log_revert, source_user))
            if not self.dry_run:
                # after successful proxy change, append username to dovecot_migration_users.done
                # on a revert, remove it from dovecot_migration_users.done
                file_done.append_line(source_user) if not self.revert else file_done.remove_line(source_user)
                # kick imap connections on old server (Cyrus) before running final-sync below
                host_key_old = 'source' if not self.revert else 'dest'
                self.logger.info('Kicking imap connection(s) on {} ...'.format(cfg['proxies'][host_key_old]['server']))
                kick_imap_connection(cfg['proxies'][host_key_old]['server'], cfg[host_key_old]['vendor'], source_user)

        if self.skip_postsync:
            self.logger.info('Skipping data post-sync for user {}'.format(source_user))
        else:
            self.logger.info('Running {}data final-sync for user {}'.format(log_revert, source_user))
            with Profiling(profiler, 'postsync'):
                data_migrator.migrate_data(False, self.revert)
            self.logger.info('{}Final-Sync for user {} finished'.format(log_revert, source_user))

        self.logger.info('###### END {}migration of user {} ######'.format(log_revert, source_user))
        profiler.store()
        return True


if __name__ == '__main__': #pragma: no cover
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('--users-file', default=cfg['migrate']['users_file'], help='File which contains the usernames to migrate (one per line). (Default: {})'.format(cfg['migrate']['users_file']))
    parser.add_argument('--username', default=None, help='If given only this user will be migrated instead of the content of the users file. Source and destination username must be the same.')
    parser.add_argument('--revert', action='store_true', help='Do a revert migration of the specified user(s).')
    parser.add_argument('--dry-run', action='store_true', help='Only log what would happen and show sieve errors. Should be used with --debug')
    parser.add_argument('--logdir', default=cfg['logdir'], help='Logdir path (Default: {})'.format(cfg['logdir']))
    parser.add_argument('--debug', action='store_true', help='Enable debug mode')
    parser.add_argument('--verbose', action='store_true', help='Also print all log entries')
    parser.add_argument('--skip-proxy-change', action='store_true', help='Skip migration to proxied IMAP server (Dovecot), only migrate data without going live.')
    parser.add_argument('--skip-presync', action='store_true', help='Skip first data synchronization step, only execute the second incremental one')
    parser.add_argument('--skip-postsync', action='store_true', help='Skip final data synchronization step')
    parser.add_argument('--skip-sync-sieve', action='store_true', help='Skip synchronization of Sieve script')
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(path.basename(__file__), args.logdir, args.verbose, args.debug, cfg)

    migrator = Migrator(logger, args.revert, args.dry_run, args.skip_proxy_change, args.skip_presync, args.skip_postsync, args.skip_sync_sieve)
    if not args.username:
        # Use lockfile to ensure only one instance of migrate.py is running
        with SimpleFlock(path.realpath(__file__) + '.lock', 3):
            logger.info('### START migration of users in file {} ###'.format(args.users_file))
            migrator.run_migration(args.users_file)
            logger.info('### END migration of users in file {} ###'.format(args.users_file))
    else:
        migrator.migrate_user(args.username, args.username, args.users_file)
