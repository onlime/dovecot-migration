#!/usr/bin/env python3
import argparse
from helper.setup_logging import configure_logging
from os import path
from requests import post
from settings import get_settings
from subprocess import check_call, CalledProcessError

cfg = get_settings()

def kick_imap_connection(server: str, vendor: str, username: str):
    if vendor == 'dovecot':
        # kicking connections by user
        # https://wiki.dovecot.org/Tools/Doveadm/Kick
        try:
            check_call(['/usr/bin/doveadm', 'kick', '-f', username])
        except CalledProcessError as e:
            # Returns exit code 68 if "no users kicked" which should not raise an exception.
            if e.returncode != 68:
                raise e
    elif vendor == 'cyrus':
        # kill nginx worker to release connections (nginx restart would also be possible [!reload does not work in this case])
        cmd = ['/usr/local/sbin/nginx-kill-imap-by-user.sh', username]
        check_call([ 'ssh', '-qt', server ] + cmd)

def change_proxy(username: str, proxy_key: str, dry_run: bool, logger: object):
    proxy_server = cfg['proxies'][proxy_key]['server']
    proxy_port_imap = cfg['proxies'][proxy_key]['port_imap']
    proxy_port_pop3 = cfg['proxies'][proxy_key]['port_pop3']

    logger.debug('Will change proxy config of {} to: proxy_server => {}, proxy_port_imap => {}, proxy_port_pop3 => {}'.format(username, proxy_server, proxy_port_imap, proxy_port_pop3))

    data = {
        'apikey': cfg['proxies']['apikey'],
        'username': username,
        'proxy_server': proxy_server,
        'proxy_port_imap': proxy_port_imap,
        'proxy_port_pop3': proxy_port_pop3
        }
    if not dry_run:
        output = post(cfg['proxies']['apiurl'], data).text
    else:
        output = 'Nothing changed - Dry-Run'
    logger.debug('API response: {}'.format(output))
    return output


if __name__ == '__main__': #pragma: no cover
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('username', help='Username of the account which should be synced')
    parser.add_argument('proxy_key', choices=['source', 'dest'], help='Server configuration key to which this user should be proxied.')
    parser.add_argument('--dry-run', action='store_true', help='Only log what would happen. Should be used with --debug')
    parser.add_argument('--logdir', default=cfg['logdir'], help='Logdir path (Default: {})'.format(cfg['logdir']))
    parser.add_argument('--debug', action='store_true', help='Enable debug mode')
    parser.add_argument('--verbose', action='store_true', help='Also print all log entries')
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(path.basename(__file__), args.logdir, args.verbose, args.debug, cfg)

    logger.info('### START changing proxy for user {} ###'.format(args.username))
    change_proxy(args.username, args.proxy_key, args.dry_run, logger)
    logger.info('### END changing proxy for user {} ###'.format(args.username))
