import MySQLdb

class DbHelper:

    def __init__(self, config: object):
        self.config = config
        self.db = self.connect()
        self.cur = self.db.cursor()

    def connect(self):
        db = MySQLdb.connect(
            host=self.config['db_host'], 
            user=self.config['db_user'], 
            passwd=self.config['db_pass'], 
            db=self.config['db_name'], 
            use_unicode=True,
            charset="utf8"
        )
        return db

    def store_migration(self, data: object):
        query = 'INSERT INTO `migrations` (`{}`) VALUES ({})'.format(
            '`, `'.join(data.keys()),
            ', '.join(['%s'] * len(data))
        )
        self.cur.execute(query, data.values())
        self.db.commit()
