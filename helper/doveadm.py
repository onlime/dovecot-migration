import sys
from subprocess import check_output

def get_quota_storage(username: str = None):
    cmd = "doveadm -f tab quota get -u {} | grep STORAGE | cut -f3".format(username)
    output = check_output(cmd, shell=True).decode(sys.stdout.encoding)
    return int(output)
