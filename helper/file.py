from os.path import isfile
from simpleflock import SimpleFlock

class File:

    def __init__(self, filename: str):
        self.filename = filename
        self.lockfile = filename + '.lock'
        self.lock_timeout = 3

    def get_lines(self):
        with open(self.filename) as f:
            lines = f.read().splitlines()
        return lines

    def line_in_file(self, value: str):
        if not isfile(self.filename):
            return False
        return value in self.get_lines()

    def append_line(self, value: str):
        # Gain lock so that appending line does not conflict with 
        # removal at the same time.
        with SimpleFlock(self.lockfile, self.lock_timeout):
            with open(self.filename, 'a') as f:
                f.write(value + "\n")

    def remove_line(self, value: str, silent: bool = True):
        # Gain lock so that removing line does not conflict with 
        # appending at the same time.
        with SimpleFlock(self.lockfile, self.lock_timeout):
            if not isfile(self.filename):
                return False
            lines = self.get_lines()
            # ignore inexistent lines if silent is enabled
            if silent and value not in lines:
                return False
            lines.remove(value)
            with open(self.filename, 'w') as f:
                f.writelines("{}\n".format(line) for line in lines)
            return True
