import logging
from uuid import uuid4
from sys import stdout, stderr
from os import path
from webhook_logger.slack import SlackHandler, SlackFormatter

RUN_ID = str(uuid4())[:8]
DEFAULT_LOG_LEVEL = logging.INFO

def configure_logging(curr_script: str, logdir: str, verbose: bool, debug: bool, cfg: object):
    log_level = logging.DEBUG if debug else DEFAULT_LOG_LEVEL
    log_format = '%(asctime)s %(levelname)s (' + RUN_ID + ') ' + curr_script + ' - %(message)s'

    # Configure global logging for extended.log, including messages from 3rd party modules (e.g. managesieve3)
    logfile_extended = path.join(logdir, 'extended.log')
    log_handlers = [logging.FileHandler(filename=logfile_extended)]
    if verbose:
        log_handlers.append(logging.StreamHandler(stdout))
    logging.basicConfig(level=log_level, format=log_format, handlers=log_handlers)

    # migration.log for project specific logging
    logger = logging.getLogger(__name__)
    formatter = logging.Formatter(log_format)
    logfile = path.join(logdir, 'migration.log')
    log_handler_migration = logging.FileHandler(filename=logfile)
    log_handler_migration.setFormatter(formatter)
    logger.addHandler(log_handler_migration)
    
    # log messages above WARNING to stderr
    log_handler_stderr = logging.StreamHandler(stderr)
    log_handler_stderr.setLevel(logging.WARNING)
    logger.addHandler(log_handler_stderr)

    # Add Slack handler
    if cfg.get('log_slack_enabled', False):
        slack_handler = SlackHandler(hook_url=cfg['log_slack_webhook'])
        slack_handler.setLevel(logging.WARN)
        slack_handler.formatter = SlackFormatter(log_format)
        logger.addHandler(slack_handler)

    return logger
