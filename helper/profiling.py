from time import time
from helper.db_helper import DbHelper
from helper.doveadm import get_quota_storage

class Profiler:
    def __init__(self, config, source_user: str, dest_user: str, run_id: str, reverse: bool = False):
        self.config = config
        self.source_user = source_user
        self.dest_user = dest_user
        self.run_id = run_id
        self.reverse = reverse
        self.timers = {}
        self.profiles = {}
        self.timer_keys = [
            'presync',
            'sievesync',
            'proxychange',
            'postsync',
            'total'
        ]
        # start default 'total' timer for total runtime
        self.start_timer('total')
    
    def verify_timer_key(self, key: str):
        if key not in self.timer_keys:
            raise ValueError("'{}' is no valid timer key.".format(key))

    def start_timer(self, key: str):
        self.verify_timer_key(key)
        self.timers[key] = time()
    
    def stop_timer(self, key: str):
        self.verify_timer_key(key)
        self.profiles[key] = time() - self.timers[key]
    
    def store(self):
        # stop default 'total' timer for total runtime
        self.stop_timer('total')
        data = {
            'source_user': self.source_user,
            'dest_user': self.dest_user,
            'run_id': self.run_id,
            'presync_time': self.profiles.get('presync', 0),
            'sievesync_time': self.profiles.get('sievesync', 0),
            'proxychange_time': self.profiles.get('proxychange', 0),
            'postsync_time': self.profiles.get('postsync', 0),
            'total_time': self.profiles.get('total', 0),
            'storage_kb': get_quota_storage(self.dest_user),
            'reverse': int(self.reverse)
        }
        dbhelper = DbHelper(self.config)
        dbhelper.store_migration(data)

class Profiling:
   """Decorator, intended for use with the `with` syntax. It will start/stop timers."""
   def __init__(self, profiler: Profiler, key: str = 'total'):
      self.profiler = profiler
      self.key = key

   def __enter__(self):
       self.profiler.start_timer(self.key)

   def __exit__(self, *args):
       self.profiler.stop_timer(self.key)
