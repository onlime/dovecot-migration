#!/usr/bin/env python3
import argparse
from helper.setup_logging import configure_logging
from subprocess import check_output, CalledProcessError
from os import path

from settings import get_settings

cfg = get_settings()

class DataMigrator(object):
    """Class which represents the whole data migrator"""

    def __init__(self, source_user: str, dest_user: str, logger: object, dry_run: bool = False):
        self.source_user = source_user
        self.dest_user = dest_user
        self.dry_run = dry_run
        self.logger = logger

    def build_imapsync_cmd(self, delete: bool = False, revert: bool = False):
        src_string = 'source' if not revert else 'dest'
        dest_string = 'dest' if not revert else 'source'

        cmd = ['/usr/bin/imapsync']

        # Args for host1
        cmd.extend(['--host1', cfg[src_string]['host']])
        cmd.extend(['--port1', str(cfg[src_string]['imap_port'])])
        cmd.append('--ssl1')
        cmd.extend(['--user1', self.source_user])
        cmd.extend(['--authuser1', cfg[src_string]['authuser']])
        cmd.extend(['--passfile1', cfg[src_string]['passfile']])
        
        # Args for host2
        cmd.extend(['--host2', cfg[dest_string]['host']])
        cmd.extend(['--port2', str(cfg[dest_string]['imap_port'])])
        cmd.append('--ssl2')
        cmd.extend(['--user2', self.dest_user])
        cmd.extend(['--authuser2', cfg[dest_string]['authuser']])
        cmd.extend(['--passfile2', cfg[dest_string]['passfile']])
        
        cmd.extend(['--pidfile', self.source_user + '.pid', '--pidfilelocking'])
        cmd.extend(['--logdir', cfg['logdir']])

        if delete:
            cmd.append('--delete2')
            cmd.append('--delete2folders')
        if self.dry_run:
            cmd.append('--dry')

        self.logger.info('Using the following command to sync data: "{}"'.format(' '.join(cmd)))
        return cmd
    
    def build_dsync_cmd(self, delete: bool = False):
        """
        Initial full sync (deleting/reverting any changes in destination if necessary):
        $ doveadm -o mail_fsync=never backup -R -u <mailaccount> imapc:
        Incremental one-way merge (it's ok to do changes on both sides):
        $ doveadm -o mail_fsync=never sync -1 -R -u <mailaccount> imapc:
        """
        doveadm_cmd = '/usr/bin/doveadm'
        if not path.isfile(doveadm_cmd):
            raise FileNotFoundError('{} was not found. Might want to switch from dsync to imapsync migration_method.'.format(doveadm_cmd))
        cmd = [doveadm_cmd]
        cmd.extend(['-o', 'mail_fsync=never'])
        if self.dest_user != self.source_user:
            # override imapc_user/pop3c_user if source_user is different from dest_user
            cmd.extend(['-o', 'imapc_user={}'.format(self.source_user), '-o', 'pop3c_user={}'.format(self.source_user)])
        if delete:
            cmd.append('backup')
        else:
            cmd.extend(['sync', '-1'])
        cmd.extend(['-R', '-u', self.dest_user, 'imapc:'])

        self.logger.info('Using the following command to sync data: "{}"'.format(' '.join(cmd)))
        return cmd

    def migrate_data(self, delete: bool = False, revert: bool = False):
        if revert or cfg['data']['migration_method'] == 'imapsync':
            cmd = self.build_imapsync_cmd(delete, revert)
            if self.dry_run:
                self.logger.info('Beware, the following is only a dry-run, using imapsync --dry flag ...')
            try:
                check_output(cmd)
                self.logger.info('imapsync successfully done. See logfile in {} for user {}'.format(cfg['logdir'], self.source_user))
            except Exception as e:
                self.logger.error('An error occurred during imapsync run. See logfile in {} for user {}'.format(cfg['logdir'], self.source_user))
                raise e
        else: # default migration_method: dsync
            cmd = self.build_dsync_cmd(delete)
            if self.dry_run:
                self.logger.info('Beware, this is only a dry-run, not executing dsync ...')
            else:
                try:
                    check_output(cmd)
                    self.logger.info('dsync successfully done for user {}'.format(self.source_user))
                except Exception as e:
                    self.logger.error('An error occurred during dsync run for user {}'.format(self.source_user))
                    raise e

if __name__ == '__main__': #pragma: no cover
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('username', help='Username of the account which should be synced. Source and destination username must be the same.')
    parser.add_argument('--delete', action='store_true', help='Sync all mails with deleting the current content of the target.')
    parser.add_argument('--revert', action='store_true', help='Do a reverse sync of the mail data of the user')
    parser.add_argument('--logdir', default=cfg['logdir'], help='Logdir path (Default: {})'.format(cfg['logdir']))
    parser.add_argument('--dry-run', action='store_true', help='Only log what would happen and show sieve errors. Should be used with --debug')
    parser.add_argument('--debug', action='store_true', help='Enable debug mode')
    parser.add_argument('--verbose', action='store_true', help='Also print all log entries')
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(path.basename(__file__), args.logdir, args.verbose, args.debug, cfg)

    migrator = DataMigrator(args.username, args.username, logger, args.dry_run)
    migrator.migrate_data(args.delete, args.revert)
