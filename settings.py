from yaml import safe_load
from os.path import isfile
from collections.abc import Mapping

DEFAULT_CFG = 'config/default.yml'
ENV_CFG = 'config/environment.yml'

def get_settings():
    with open(DEFAULT_CFG) as default_yml:
        cfg = safe_load(default_yml)

    if isfile(ENV_CFG):
        with open(ENV_CFG) as env_yml:
            env_cfg = safe_load(env_yml)
            dict_merge(cfg, env_cfg)

    return cfg

# https://gist.github.com/angstwad/bf22d1822c38a92ec0a9
def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k in merge_dct:
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]
