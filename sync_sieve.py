#!/usr/bin/env python3
import argparse
from helper.setup_logging import configure_logging
from os import path
from time import sleep
import managesieve3
import re
from settings import get_settings

cfg = get_settings()

class SieveMigrator(object):
    """Class which represents the whole sieve migrator"""

    def __init__(self, source_user: str, dest_user: str, logger: object, dry_run: bool):
        self.source_user = source_user
        self.dest_user = dest_user
        self.dry_run = dry_run
        self.logger = logger

        # source host Sieve connection
        src_vendor = cfg['source']['vendor']
        src_authuser = cfg['source']['authuser'] if src_vendor == 'cyrus' else '{}*{}'.format(source_user, cfg['source']['authuser'])
        src_password = self.get_password(cfg['source']['passfile'])
        self.src_con = self.sieve_login(cfg['source']['host'], cfg['source']['sieve_port'], source_user, src_authuser, src_password)

        # dest host Sieve connection
        dest_vendor = cfg['dest']['vendor']
        dest_authuser = cfg['dest']['authuser'] if dest_vendor == 'cyrus' else '{}*{}'.format(dest_user, cfg['dest']['authuser'])
        dest_password = self.get_password(cfg['dest']['passfile'])
        self.dest_con = self.sieve_login(cfg['dest']['host'], cfg['dest']['sieve_port'], dest_user, dest_authuser, dest_password)

    def sieve_login(self, host: str, port: int, username: str, authuser: str, password: str):
        sieve = managesieve3.Managesieve(host, int(port))
        sieve.cmd_starttls()
        sieve.login_plain(username, authuser, password)
        self.logger.debug('Logged in on {} with user {} and authuser {}'.format(host, username, authuser))
        return sieve

    def get_password(self, file):
        if path.isfile(file):
            with open(file) as file_s:
                pw = file_s.read()
        else:
            self.logger.error('Password file {} not found.'.format(file))
            raise FileNotFoundError(file)
        
        return pw

    def delete_sieve(self):
        scripts, active = self.dest_con.cmd_listscripts()
        self.logger.debug('Currently available scripts on dest: {}; Currently active: {}'.format(', '.join(scripts), active))
        self.dest_con.cmd_setactive(None)
        for script in scripts:
            self.logger.debug('Deleting sieve script {} on dest.'.format(script))
            if not self.dry_run:
                self.dest_con.cmd_deletescript(script)

    def get_active_script(self):
        available_scripts, active = self.src_con.cmd_listscripts()
        if not active: # no active script was found
            return None, None
        self.logger.debug('List of scripts on src: {}; Currently active: {}'.format(', '.join(available_scripts), active))
        if active != cfg['sieve']['default_active']:
            self.logger.warn('A different script than {} is currently active and will be migrated for user {}: {}'.format(cfg['sieve']['default_active'], self.source_user, active))
        script = self.src_con.cmd_getscript(active)
        self.logger.debug('Got this script from src: {}'.format(active))
        return script, active

    def write_script(self, script: str, name: str):
        converted_script = self.convert_script(script)
        try:
            if self.dry_run:
                self.dest_con.cmd_checkscript(converted_script)
                self.logger.debug('Tested script {} successfully.'.format(name))
            else:
                self.dest_con.cmd_putscript(name, converted_script)
                self.logger.debug('Wrote script {} to dest.'.format(name))
        except managesieve3.ServerResponseNo as e:
            self.logger.warn('Error writing sieve script {} on {} ({}): {}'.format(name, self.dest_user, cfg['dest']['host'], e.text))
        self.force_activate_script(name)
    
    def force_activate_script(self, name: str):
        # Trying 10 times to activate Sieve script as first time SETACTIVE sieve command might fail and and result
        # in a managesieve3.ServerResponseNo excaption.
        # see https://gitlab.onlime.ch/onlime/dovecot-migration/issues/15
        tries = 10
        for i in range(tries):
            try:
                self.dest_con.cmd_setactive(name)
                self.logger.debug('Activated script {} on {} ({}).'.format(name, self.dest_user, cfg['dest']['host']))
                return True
            except managesieve3.ServerResponseNo:
                if i < (tries - 1):
                    sleep(1)
                    self.logger.debug('Trying again to activate sieve script ...')
        self.logger.warn('Error activating sieve script {} on {} ({}) after {} tries.'.format(name, self.dest_user, cfg['dest']['host'], tries))
        return False

    def convert_script(self, script: str):
        # replace deprecated Sieve capabilities by its newer versions
        # see https://wiki.dovecot.org/Pigeonhole/Sieve
        script = script.replace('"imapflags"', '"imap4flags"', 1)
        script = script.replace('"notify"', '"enotify"', 1)
        
        ## regex search-replacement to support new enotify format, see #22
        #if 'notify ' in script:
        #    script = re.sub(r':method "(.*)"(.*) :options "(.*)" (.*);', r'\2\4 "\1:\3";', script)
        
        # remove INBOX prefix for fileinto on Dovecot
        if cfg['source']['inbox_prefix'] and not cfg['dest']['inbox_prefix']:
            return script.replace('fileinto "INBOX/', 'fileinto "')
        else:
            return script
    
    def migrate_active(self, delete: bool = True):
        if delete:
            self.delete_sieve()
        script, active = self.get_active_script()
        if active:
            dest_active = cfg['sieve']['default_active'] if cfg['sieve']['migrate_to_default_active'] else active
            self.write_script(script, dest_active)
        else:
            self.logger.info('Not migrating Sieve script from source user {} as no active script was found. This is OK.'.format(self.source_user))
    
    def logout(self):
        # Only report logout failures as INFO log messages, as cmd_logout() does not seem to be supported on Dovecot
        try:
            self.src_con.cmd_logout()
        except:
            self.logger.info('Was not able to logout from sieve source {}'.format(cfg['source']['host']))

        try:
            self.dest_con.cmd_logout()
        except:
            self.logger.info('Was not able to logout from sieve destination {}'.format(cfg['dest']['host']))

if __name__ == '__main__': #pragma: no cover
    # argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('username', help='Username of the account which should be synced. Source and destination username must be the same.')
    parser.add_argument('--delete', action='store_true', help='Delete sieve rules at the destination before migrating.')
    parser.add_argument('--dry-run', action='store_true', help='Only log what would happen and show sieve errors. Should be used with --debug')
    parser.add_argument('--logdir', default=cfg['logdir'], help='Logdir path (Default: {})'.format(cfg['logdir']))
    parser.add_argument('--debug', action='store_true', help='Enable debug mode')
    parser.add_argument('--verbose', action='store_true', help='Also print all log entries')
    args = parser.parse_args()

    # Configure logging
    logger = configure_logging(path.basename(__file__), args.logdir, args.verbose, args.debug, cfg)

    logger.info('### START migration of sieve scripts for user {} ###'.format(args.username))
    migrator = SieveMigrator(args.username, args.username, logger, args.dry_run)
    migrator.migrate_active(args.delete)
    migrator.logout()
    logger.info('### END migration of sieve scripts for user {} ###'.format(args.username))
